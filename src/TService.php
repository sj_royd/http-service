<?php

namespace SJRoyd\HTTPService;

trait TService
{
    protected $test;
    protected $debug;

    /**
     * SSL certificate file path
     *
     * @var string
     */
    protected $ssl_cert;
    /**
     * WebService path
     *
     * @var string
     */
    protected $ws_path;

    /**
     * WebService endpoint group name
     *
     * @var string
     */
    protected $ws_name;

    /**
     * @var int
     */
    protected $responseStatusCode;

    /**
     * @var \JsonMapper
     */
    protected $mapper;

    /**
     * @var string
     */
    private $rawData;

    public function prepareMapper()
    {
        $this->mapper = new \JsonMapper();
    }

    /**
     * @return string
     */
    public function getRawData()
    {
        return $this->rawData;
    }

    /**
     * Parses the response to the specified object
     *
     * @param  int       $code         Server response status code
     * @param  string    $contentType  Response content type
     * @param  string    $data         Response content data
     * @param  object[]  $cast         List of objects to cast depends of response status code
     *
     * @return mixed
     * @throws \JsonMapper_Exception
     */
    protected function parseResponse($code, $contentType, $data, $cast = null)
    {
        $this->rawData = $data;

        if ($cast && ($mapClass = $this->getCastObj($cast, $code))) {
            $contentType = explode(';', $contentType)[0];
            switch ($contentType) {
                case 'xml':
                case 'application/xml':
                    $xml  = new JsonXMLElement($data);
                    $data = json_encode($xml);
                // non-break, cause now it's a json and can be casting below
                case 'json':
                case 'application/json':
                    $data = json_decode($data);
                    break;
            }

            $this->mapper || $this->prepareMapper();

            if (is_array($data)) {
                $data = $this->mapper->mapArray($data, [], $mapClass);
            } elseif (is_object($data)) {
                $data = $this->mapper->map($data, new $mapClass);
            }
        }

        return $data;
    }

    /**
     *
     * @param  object[]  $cast
     * @param  int       $code
     *
     * @return boolean|object
     */
    protected function getCastObj($cast, $code)
    {
        foreach ($cast as $codePattern => $obj) {
            $pattern = "~{$codePattern}~";
            preg_match($pattern, $code, $m);
            if ($m) {
                return $obj;
            }
        }

        return false;
    }
}