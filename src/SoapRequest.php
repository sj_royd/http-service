<?php

namespace SJRoyd\HTTPService;

use GuzzleHttp\Psr7\Message;

class SoapRequest
{
    use TService;

    protected $soapVersion = SOAP_1_2;
    protected $wsdl = true;
    protected $wsdl_config = [];
    /**
     * @var \SoapClient
     */
    protected $client;

    /**
     * SoapRequest constructor.
     *
     * @param  bool         $test
     * @param  bool         $debug
     * @param  string|null  $client
     *
     * @throws \Exception
     */
    public function __construct($test = false, $debug = false, $client = null)
    {
        ! $client && $client = \SoapClient::class;
        $this->test  = $test;
        $this->debug = $debug;

        $this->prepareMapper();

        $location = $this->ws_path;
        $this->ws_name && $location .= "/{$this->ws_name}";
        $default      = [
            'location'           => $location,
            'uri'                => ' ',
            'trace'              => 1,
            'connection_timeout' => 30,
            'soap_version'       => $this->soapVersion,
//			'cache_wsdl' => WSDL_CACHE_NONE
        ];
        $config       = array_merge($default, $this->wsdl_config);
        $this->client = new $client($this->wsdl ? $location.'?wsdl' : null, $config);
        if ( ! ($this->client instanceof \SoapClient)) {
            throw new \Exception('Client is invalid SoapClient instance');
        }
    }

    /**
     * Call WebService action
     *
     * @param  string  $action     The name of the SOAP function to call
     * @param  array   $args       An array of the arguments to pass to the function
     * @param  array   $cast
     * @param  array   $options    An associative array of options to pass to the client
     * @param  array   $headersIn  An array of headers to be sent along with the SOAP request
     *
     * @return mixed
     * @throws \SoapFault
     */
    public function call(
        $action,
        $args = [],
        $cast = null,
        $options = null,
        $headersIn = null
    ) {
        if ($this->debug) {
            echo "Called method: {$action}".PHP_EOL;
            echo "Sent params: ";
            print_r($args);
        }
        $response = $this->client->__soapCall($action, (array)$args, $options,
            $headersIn);

        $headers = Message::parseResponse(
            $this->client->__getLastResponseHeaders().PHP_EOL
        );

        $this->responseStatusCode = $headers->getStatusCode();

        return $this->parseResponse(
            $this->responseStatusCode, null, $response, $cast
        );
    }
}