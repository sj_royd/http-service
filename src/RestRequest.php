<?php

namespace SJRoyd\HTTPService;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class RestRequest
{
    use TService;

    /**
     * @var Client
     */
    protected $client;

    public function __construct($test = false, $debug = false)
    {
        $this->test   = $test;
        $this->debug  = $debug;
        $this->client = new Client();

        $this->prepareMapper();
    }

    /**
     * Call WebService action
     *
     * @param  string  $action
     * @param  array   $params
     * @param  array   $cast
     *
     * @return mixed
     * @throws \Exception
     */
    public function call($action = '', $params = [], $cast = null)
    {
        $default = [
            'method' => 'GET',
            'verify' => $this->ssl_cert,
        ];
        $params  = array_merge($default, $params);

        try {
            $location = $this->ws_path;
            $this->ws_name && $location .= "/{$this->ws_name}";
            $action && $location .= "/{$action}";
            $response = $this->client->request($params['method'], $location, $params);
        } catch (BadResponseException $ex) {
            $response = $ex->getResponse();
        }

        $this->responseStatusCode = $response->getStatusCode();

        return $this->parseResponse(
            $this->responseStatusCode,
            $response->getHeaderLine('Content-type'),
            $response->getBody()->getContents(),
            $cast
        );
    }

    /**
     * Call GET method of WebService action
     *
     * @param  string  $action
     * @param  array   $params
     * @param  array   $cast
     *
     * @return mixed
     * @throws \Exception
     */
    public function callGet($action = '', $params = [], $cast = null)
    {
        $params['method'] = 'GET';

        return $this->call($action, $params, $cast);
    }

    /**
     * Call POST method of WebService action
     *
     * @param  string  $action
     * @param  array   $params
     * @param  array   $cast
     *
     * @return mixed
     * @throws \Exception
     */
    public function callPost($action = '', $params = [], $cast = null)
    {
        $params['method'] = 'POST';

        return $this->call($action, $params, $cast);
    }

    /**
     * Call PUT method of WebService action
     *
     * @param  string  $action
     * @param  array   $params
     * @param  array   $cast
     *
     * @return mixed
     * @throws \Exception
     */
    public function callPut($action = '', $params = [], $cast = null)
    {
        $params['method'] = 'PUT';

        return $this->call($action, $params, $cast);
    }

    /**
     * Call PUT method of WebService action
     *
     * @param  string  $action
     * @param  array   $params
     * @param  array   $cast
     *
     * @return mixed
     * @throws \Exception
     */
    public function callPatch($action = '', $params = [], $cast = null)
    {
        $params['method'] = 'PATCH';

        return $this->call($action, $params, $cast);
    }

    /**
     * Call DELETE method of WebService action
     *
     * @param  string  $action
     * @param  array   $params
     * @param  array   $cast
     *
     * @return mixed
     * @throws \Exception
     */
    public function callDelete($action = '', $params = [], $cast = null)
    {
        $params['method'] = 'DELETE';

        return $this->call($action, $params, $cast);
    }

}