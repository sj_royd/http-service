# HTTP Service

Simple library for HTTP requests via REST or SOAP service.

# Usage

## SOAP service

```php
<?php 
// optional Client
class Client extends \SoapClient
{
    /**
     * @var string
     */
    private $headerNs = 'http://www.w3.org/2005/08/addressing';

    /**
     * @var resource
     */
    private $context;

    public function __construct($wsdl, array $options = [])
    {
        $this->context = stream_context_create();
        $options['stream_context'] = $this->context;
        parent::__construct($wsdl, $options);
    }

    /**
     * @param   string  $action
     * @param   array   $args
     * @param   null    $options
     * @param   null    $in_heads
     * @param   null    $out_heads
     *
     * @return mixed
     */
    public function __soapCall($action, $args, $options = null, $in_heads = null, &$out_heads = null)
    {
        $in_heads = $this->getRequestHeaders($action);

        return parent::__soapCall($action, [$args], $options, $in_heads, $out_heads);
    }

    /**
     * @param   string  $request
     * @param   string  $location
     * @param   string  $action
     * @param   int     $version
     * @param   int     $one_way
     *
     * @return string
     */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        $response = parent::__doRequest($request, $location, $action, $version, $one_way);

        return stristr(stristr($response, '<s:'), '</s:Envelope>', true) . '</s:Envelope>';;
    }

    /**
     * @param $myData
     */
    public function setMyHeader($myData)
    {
        stream_context_set_option($this->context, [
            'http' => ['header' => "my-data: {$myData}"]
        ]);
    }

    /**
     * @param $data
     *
     * @return array
     */
    private function getRequestHeaders($data)
    {
        return [
            new \SoapHeader($this->headerNs, 'Name', $data)
        ];
    }
}

class MyService extends SJRoyd\HTTPService\SoapRequest 
{
    protected $ws_path = 'https://sample.service.com';
    protected $ws_name = 'service1'; // https://sample.service.com/service1
    protected $soapVersion = SOAP_1_1; // default SOAP_1_2
    protected $wsdl = true; // default
    protected $wsdl_config = []; // SOAP WSDL options

    // optional Client injection 
    public function __construct($test = false, $debug = false) 
    {
        parent::__construct($test, $debug, Client::class);
        $this->client->__setLocation('My_Location_Data');
    }

    public function callAction()
    {
        $this->client->setMyHeader('some-data');
        $body = []; // action body params as array or an array convertible object
        $cast = [
            200   => Some\Response\Class::class,
            '4..' => Some\Response\Error::class // any 400+ status code response    
        ];
        $response = $this->call('actionName', $body, $cast);
    }
}
```

## REST service

```php
<?php

class MyService extends SJRoyd\HTTPService\RestRequest
{
    protected $ws_path = 'https://sample.service.com/api';
}

class MyServiceAction extends MyService
{
    protected $ws_name = 'service1'; // https://sample.service.com/api/service1

    public function getAction()
    {
        $response = $this->callGet('getAction', [], [
            200   => Some\Response\Unit::class,
            401   => Some\Response\Error\Unauthorized::class,
            '4..' => Some\Response\Error::class // any 400+ status code response    
        ]);

        if($response instanceof \Exception){
            throw new \Exception($response->getMessage(), $this->responseStatusCode);
        }
        return $response;
    }

    public function putAction(array $data){
        $params = [ // Guzzle request params
            'json' => $data
        ];

        $cast = [
            401   => Some\Response\Error\Unauthorized::class,
            '5..' => Some\Response\Error::class // any 500+ status code response    
        ];
    
        $response = $this->callPut('putAction', $params, $cast);

        if($response instanceof \Exception){
            throw new \Exception($response->getMessage(), $this->responseStatusCode);
        }
        return true;
    }

    public function postAction(array $data){
        $params = [ // Guzzle request params
            'method' => 'POST',
            'json' => $data
        ];

        $cast = [
            200   => Some\Response\Unit::class,
            401   => Some\Response\Error\Unauthorized::class,
            '5..' => Some\Response\Error::class // any 500+ status code response    
        ];
    
        $response = $this->call('postAction', $params, $cast);

        if($response instanceof \Exception){
            throw new \Exception($response->getMessage(), $this->responseStatusCode);
        }
        return $response;
    }
}
```

RestRequest have methods for GET, POST, PUT, PATCH, and DELETE request and they calls `callGet()`, `callPost()`, `callPut()`, `callPatch()` and `callDelete()`. You can also use the `call()` method and send HTTP method in params array like `$params = ['method' => 'PUT']` but default method is GET.

### PHP 8.x named arguments

```php
<?php

class MyService extends SJRoyd\HTTPService\RestRequest
{
    protected $ws_path = 'https://sample.service.com/api';
}

class MyServiceAction extends MyService
{
    protected $ws_name = 'service1'; // https://sample.service.com/api/service1

    public function getAction()
    {
        $response = $this->callGet( // call GET https://sample.service.com/api/service1/getAction
            action: 'getAction',
            cast: [
                200   => Some\Response\Unit::class,
                401   => Some\Response\Error\Unauthorized::class,
                '4..' => Some\Response\Error::class // any 400+ status code response    
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response;
        } else {
            throw $response;
        }
        return $response;
    }

    public function putAction(array $data)
    {    
        $response = $this->callPut( // call PUT https://sample.service.com/api/service1/putAction
            action: 'putAction',
            params: [ // Guzzle request params
                'json' => $data
            ],
            cast: [
                401   => Some\Response\Error\Unauthorized::class,
                '5..' => Some\Response\Error::class // any 500+ status code response    
            ]
        );

        if ($this->responseStatusCode == 200) {
            return true;
        } else {
            throw $response;
        }
    }

    public function postAction(array $data)
    {
        $params = [ // Guzzle request params
            'method' => 'POST',
            'json' => $data
        ];

        $cast = [
            200   => Some\Response\Unit::class,
            401   => Some\Response\Error\Unauthorized::class,
            '5..' => Some\Response\Error::class // any 500+ status code response    
        ];
    
        $response = $this->call(action: 'postAction', params: $params, cast: $cast); // call POST https://sample.service.com/api/service1/postAction

        if ($this->responseStatusCode == 200) {
            return $response;
        } else {
            throw $response;
        }
    }
    
    public function defaultAction(array data)
    {
        $response = $this->callPost( // call POST https://sample.service.com/api/service1
            cast: [
                401       => Some\Response\Error\Unauthorized::class,
                '400|404' => Some\Response\Error::class // 400 and 404 status code response    
            ]
        );

        if ($this->responseStatusCode == 200) {
            return $response; // raw data
        } else {
            throw $response;
        }
        return $response;
    }
}
```

### Get response raw data

After call any of service actions the raw response is always be available by calling the `getRawData()` method.

```php
<?php

$service->getAction(); // Some\Response\Unit instance
$service->getRawData(); // The raw data before casting to the object
```